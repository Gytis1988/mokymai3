import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pageObjects.LoginPage;

public class TestMokymai {
    WebDriver driver;
    WebDriverWait wait;
    static String baseURL = "https://www.deinavosbaldai.lt/";
    static String password = System.getenv("SecondTestPassword");

    @Parameters("browserName")
    @BeforeMethod
    public void setup(String browserName) {
        switch (browserName) {
            case "chrome":
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case "edge":
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;
            default:
                System.out.println("no browser specified or found by name. Running chrome as default browser");
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
        }
        driver.manage().window().maximize();
        wait = new WebDriverWait(driver, 10);
    }

    @Test
    public void testARegisterButton() {
        driver.get(baseURL + "/prisijungti");
        driver.findElement(By.cssSelector("#intuero > header > div.container.header__content.cf > div > div.col.col__menu > ul > li.header__menu-account > ul > li > a"));
        Assert.assertEquals(driver.getCurrentUrl(), baseURL + "/prisijungti");
    }

    @Test
    public void testBRegisterForm() {
        driver.get(baseURL + "/prisijungti");
        driver.findElement(By.cssSelector(".cookiesBtn")).click(); //is selenium
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.registrationForm().register("Gytis", "Laukaitis", "kazkas@kazkas.com", "8712314131", "kazkur kaune 50", "Kaunas", password, true);
    }

    @Test
    public void testCLoginPage() {
        driver.get(baseURL + "/prisijungti");
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login("greta.rusecke@gmail.com", password);
    }

    @Test
    public void testDAddToCart() {
        driver.get(baseURL + "/svetaines");
        driver.findElement(By.linkText("Sekcijos")).click();
        driver.findElement(By.cssSelector(".cookiesBtn.cookiesMessageAccept")).click();
        driver.findElement(By.cssSelector(".productData_wrap-8728 img")).click();
        driver.findElement(By.cssSelector(".core_addToCart:nth-child(2)")).click();
        driver.findElement((By.cssSelector(".header__menu-cart .icon"))).click();
        driver.findElement(By.id("cart_voucher")).click();
        driver.findElement(By.id("cart_voucher")).sendKeys("999");
        driver.findElement(By.cssSelector("#cartOption_inshop > .cartOptionIcon")).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("cart_shop")));
        WebElement selectList = driver.findElement(By.id("cart_shop"));
        selectList.click();
        Select dropDown = new Select(selectList);
        dropDown.selectByValue("VilniusNaugarduko");
        driver.findElement(By.cssSelector(".cartField-firstName")).click();
        driver.findElement(By.id("buyer_firstName")).sendKeys("Greta");
        driver.findElement(By.id("buyer_lastName")).sendKeys("Son");
        driver.findElement(By.id("buyer_phone")).sendKeys("989898");
        driver.findElement(By.id("buyer_email")).click();
        driver.findElement(By.id("buyer_email")).sendKeys("gg@gg.com");
        driver.findElement(By.id("buyer_agreeToRules")).click();
    }

    @Test
    public void TestFLogOut() {
        driver.get(baseURL + "/prisijungti");
        LoginPage loginPage = new LoginPage(driver, wait);
        loginPage.login("g.laukaitis@kiro.tech", password);
        driver.findElement(By.cssSelector("button[type='submit']"));
        wait.until(ExpectedConditions.urlToBe(baseURL));
        Actions actions = new Actions(driver);
        actions.moveToElement(driver.findElement(By.cssSelector("a[title='Paskyra']")));
        actions.build().perform();
        driver.findElement(By.linkText("Atsijungti")).click();
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }
}