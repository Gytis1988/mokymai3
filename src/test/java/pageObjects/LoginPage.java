package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage {
    private final By emailBy = By.id("email");
    private final By passwordBy = By.id("password");
    private final By rememberMeBy = By.cssSelector(".formItem > .checkbox");
    private final By loginFormBy = By.className("login");

    WebDriver driver;
    WebDriverWait wait;

    public LoginPage(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
        driver.findElement(loginFormBy).findElement(By.cssSelector("button[type='submit']"));
    }

    public void login(String email, String password) {
        driver.findElement(emailBy).click();
        driver.findElement(emailBy).sendKeys(email);
        driver.findElement(passwordBy).click();
        driver.findElement(passwordBy).sendKeys(password);
        driver.findElement(rememberMeBy).click();
        driver.findElement(loginFormBy).findElement(By.cssSelector("button[type='submit']")).click();
    }

    public RegistrationForm registrationForm() {
        return new RegistrationForm(driver, wait);
    }
}
