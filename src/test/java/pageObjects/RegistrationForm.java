package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

public class RegistrationForm {

    private final By nameBy = By.name("name");
    private final By lastNameBy = By.name("lastName");
    private final By emailBy = By.name("email");
    private final By PhoneBy = By.name("profileData[phone]");
    private final By addressBy = By.name("profileData[address]");
    private final By cityBy = By.name("profileData[city]");
    private final By passwordBy = By.name("password");
    private final By passwordRepeatBy = By.name("password-repeat");
    private final By newsletterBy = By.id("newsletter");
    private final By registerBy = By.cssSelector(".btn");

    private final WebDriver driver;
    private final WebDriverWait wait;

    public RegistrationForm(WebDriver driver, WebDriverWait wait) {
        this.driver = driver;
        this.wait = wait;
    }

    public void register(String name, String lastname, String email, String phone, String address, String city, String password, boolean shouldSubscribe) {
        driver.findElement(nameBy).sendKeys(name);
        driver.findElement(lastNameBy).sendKeys(lastname);
        driver.findElement(emailBy).sendKeys(email);
        driver.findElement(PhoneBy).sendKeys(phone);
        driver.findElement(addressBy).sendKeys(address);
        driver.findElement(cityBy).sendKeys(city);
        driver.findElement(passwordBy).sendKeys(password);
        driver.findElement(passwordRepeatBy).sendKeys(password);
        if (shouldSubscribe) {
            driver.findElement(newsletterBy).click();
        }
        driver.findElement(registerBy).click();
    }
}
